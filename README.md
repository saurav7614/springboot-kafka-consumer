# Spring Boot with Kafka Consumer Example

This Project covers how to use Spring Boot with Spring Kafka to Consume JSON/String message from Kafka topics
## Start Zookeeper
- `.\bin\windows\zookeeper-server-start.bat .\config\zookeeper.properties`

## Start Kafka Server
- `.\bin\windows\kafka-server-start.bat .\config\server.properties`

## List Topics
- `.\bin\windows\kafka-topics.bat --list --zookeeper localhost:2181`

## Create Kafka Topic
- `.\bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic kafka-test`
- `.\bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic kafka-test-json`

## Describe Topic
- `.\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --describe --topic kafka-test'
- `.\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --describe --topic kafka-test-json'

## Publish to the Kafka Topic via Console
- `.\bin\windows\kafka-console-producer.bat --broker-list localhost:9092 --topic kafka-test`
		>Hi Saurav!
- `.\bin\windows\kafka-console-producer.bat --broker-list localhost:9092 --topic kafka-test-json`
		> {"name":"Saurav", "dept":"IT"}
		> {"name":"Vishal", "dept":"HR"}
		
## Delete Topic
-   `.\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --delete --topic kafka-test`
-   `.\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --delete --topic kafka-test-json`

